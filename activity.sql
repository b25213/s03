CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	title VARCHAR(50) NOT NULL,
	content VARCHAR(50) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- johnsmith@gmail.com passwordA
-- juandelacruz@gmail.com passwordB
-- janesmith@gmail.com passwordC
-- mariadelacruz@gmail.com passwordD
-- johndoe@gmail.com passwordE

-- 2021-01-01 01:00:00
-- 2021-01-01 02:00:00
-- 2021-01-01 03:00:00
-- 2021-01-01 04:00:00
-- 2021-01-01 05:00:00

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- First Code 
-- Hello World!
-- Second Code
-- Hello Earth!
-- Third Code
-- Welcome to Mars!
-- Fourth Code 
-- Bye bye solar system!

-- 2021-01-02 01:00:00
-- 2021-01-02 02:00:00
-- 2021-01-02 03:00:00
-- 2021-01-02 04:00:00
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES ("1", "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES ("1", "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES ("2", "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES ("4", "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES ("", "", "", "");

-- Get all post with author id of 1
SELECT * FROM posts WHERE user_id = 1;
-- get all users email and date time of creation
SELECT email, datetime_created FROM users; 
-- update a posts content to "Hello to the people of the earth!" where its initial content is "hello earth!" by using record's ID
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";
--delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";